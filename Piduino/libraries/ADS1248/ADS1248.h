/*
 * ADS1248.h - Library for communicating with ADS1248 chip
 * Created by Beau Piccart & Kim Gressens
 * 8 May 2012
 */
#ifndef ADS1248_h
#define ADS1248_h

#include <Arduino.h>
#include <Math.h>

class ADS1248
{
    public:
        ADS1248();
        void init();
        signed long read();
        byte readRegister(byte thisRegister);
        double readPT100Temperature();
        double readThermocoupleTemperature();
        void selectAnalogInputPair(int pair);
        void selfOffsetCal();
        bool setPGAGainAndSPS(int gain, int sps);

    private:
        void writeRegister(byte reg_addr, byte value);
        void reset();
        void setupChip();
        void calibrate();
        void readCalibrationRegisters();

};

#endif
