/*
 * ADS1248.h - Library for communicating with ADS1248 chip
 * Created by Beau Piccart & Kim Gressens
 * 8 May 2012
 */

#include <Arduino.h>
#include "ADS1248.h"
#include <SPI.h>

#define CS_pin 10
#define DRDY_pin 2
#define START_pin 8
#define RESET_pin 7

#define READ_REG  0b00100000;
#define WRITE_REG 0b01000000;

ADS1248::ADS1248()
{

}

void ADS1248::init(){
    Serial.begin(9600);

    reset();

    setupChip();

    calibrate();

    //readCalibrationRegisters();
 
    //set MUX to AIN0/1
    Serial.println("configuring MUX");
    // writeRegister(0x02,0b00110000);//onboard reference used
    writeRegister(0x02,0b00101000);//REF1 reference used
  
    //connect Vbias
    //writeRegister(0x01,0b00001111);

}    

signed long ADS1248::read() {
 digitalWrite(CS_pin,LOW);
 SPI.transfer(0b00010010);
 signed long result =  SPI.transfer(0xFF);
 result = result << 8;
 result = result | SPI.transfer(0xFF);
 result = result << 8;
 result = result | SPI.transfer(0xFF);
 result = result << 8;
 digitalWrite(CS_pin,HIGH);
 return result;
 
}


void ADS1248::writeRegister(byte reg_addr, byte value){
    byte dataToSend;
    digitalWrite(CS_pin,LOW);
  

    dataToSend = reg_addr | WRITE_REG;
    SPI.transfer(dataToSend);
    SPI.transfer(0x00);
    SPI.transfer(value);
    digitalWrite(CS_pin,HIGH);
}

double ADS1248::readThermocoupleTemperature() {


    double val = read();

    double max = 2147481344.0*64;

    //val = (val - max/2);
    double toReturn = val / max * 1.5;

    return toReturn/0.000041;

}


double ADS1248::readPT100Temperature() {
    //selectAnalogInputPair(0);

    long val = read();


    // convert to R
    double max = 2147481344.0;
    double V = (val/max)*1.5;
   // return V;
    //double R = V/0.00015;

    double R = (val / max)*1000.0; // (value / max ) * VBias / I
    



    // convert to T
    double R0 = 100;
    double A = 3.90830e-3;
    double B = -5.7750e-7;

    return (-A+sqrt(pow(A,2) - 4*B*(1-R/R0)))/(2*B);
}

bool ADS1248::setPGAGainAndSPS(int g, int sps){
	/* bit0=0
	 * bits123: 000 = 1 (default) 001 = 2 010 = 4
	   011 = 8 ; 100 = 16 ; 101 = 32 ; 110 = 64 ;111 = 128*/
	//note: should read the last bits.
	/*0001 = 10SPS
	0010 = 20SPS
	0011 = 40SPS
	0100 = 80SPS
	0101 = 160SPS
	0110 = 320SPS
	0111 = 640SPS
	1000 = 1000SPS
	1001 to 1111 = 2000SPS
	0000 = 5SPS (default)
	*/

    bool success = true;
    byte gain;
    byte spsByte;
    if (g==128) {
        gain = 0b01110000;
    } else if(g==64){
        gain = 0b01100000;
		//writeRegister(0x03,0b01100000);//64
    } else if (g==32) {
        gain = 0b01010000;
    } else if (g==16) {
        gain = 0b01000000;
    } else if (g==8) {
        gain = 0b00110000;
	}else if(g==4){
		gain = 0b00100000;
    } else if (g == 2) {
        gain = 0b00010000;
	}else if (g ==1) {
        gain = 0b00000000;
		//writeRegister(0x03,0b0001111);//1 // also set 2000SPS
	} else {
        success = false;
    }

    if (sps == 5) {
        spsByte = 0b00000000;
    } else if (sps==10) {
        spsByte = 0b00000001;
    } else if (sps == 20) {
        spsByte = 0b00000010;
    } else if (sps == 40) {
        spsByte = 0b00000011;
    } else if (sps == 80) {
        spsByte = 0b00000100;
    } else if (sps == 160) {
        spsByte = 0b00000101;
    } else if (sps == 320) {
        spsByte = 0b00000110;
    } else if (sps == 640) {
        spsByte = 0b00000111;
    } else if (sps == 1000) {
        spsByte = 0b00001000;
    } else if (sps == 2000) {
        spsByte = 0b00001111;
    } else {
        success = false;
    }

    if (success == true) {
        byte toSet = gain | spsByte;
        writeRegister(0x03, toSet);
    } else {
        Serial.println("Error setting gain and sps");
    }

    return success;
} 

byte ADS1248::readRegister(byte thisRegister) {
  digitalWrite(CS_pin,LOW);
  byte result = 0;   // result to return
   
  // SCP1000 expects the register name in the upper 6 bits
  // of the byte. So shift the bits left by two bits:
  //thisRegister = thisRegister << 2;
  // now combine the address and the command into one byte
  byte dataToSend = thisRegister | READ_REG;

 SPI.transfer(dataToSend);
 //delay(4);
  // send the device the register you want to read:
 SPI.transfer(0x01);
 //delay(4);
 // SPI.transfer(0x00);
  result = SPI.transfer(0xFF); // we want 1 - 1 bytesToRead
 //delay(4);
 

  //result = SPI.transfer(0b00000000);
  //Serial.println(result,BIN);
  // return the result:
  
  digitalWrite(CS_pin,HIGH);
  return(result);
}


void ADS1248::reset() {
    #ifdef DEBUG 
        Serial.println("RESET");
    #endif
  
    pinMode(RESET_pin,OUTPUT);
    digitalWrite(RESET_pin,LOW);
    delay(1000);
    digitalWrite(RESET_pin,HIGH);
  
}

void ADS1248::setupChip() {
 
    #ifdef DEBUG
        Serial.println("setup chip");
    #endif
      //select the chipd
      pinMode(CS_pin,OUTPUT);
      digitalWrite(CS_pin,HIGH);

      pinMode(DRDY_pin,INPUT);
      pinMode(START_pin,OUTPUT);
      digitalWrite(START_pin,HIGH);
      
      
      delay(50);
      SPI.begin();
      SPI.setBitOrder(MSBFIRST);
      SPI.setDataMode(SPI_MODE1);

      digitalWrite(CS_pin,LOW);
      delay(5);
      SPI.transfer(0x16);//stop data
      digitalWrite(CS_pin,HIGH);
      
       // set input and stuff liek that
       writeRegister(0x0A,0b00000111);
       writeRegister(0x0B,0b10001100);

       // enable Vbias for AIN2
       writeRegister(0x01,0b00000100);
     
}

void ADS1248::selectAnalogInputPair(int pair) {

    if (pair == 0) {
        writeRegister(0x00, 0b00000001);//AIN01
    } else {
        writeRegister(0x00, 0b00010011);//AIN23
    }

    delay(200);

}

void ADS1248::calibrate() {
     writeRegister(0x02,0b00110010);//internal ref on, with onboard ref for ADC
     // writeRegister(0x02,0b00101010);//internal ref on, REF1 as reference    
      
    //GAIN
    #ifdef DEBUG
    Serial.println("setting gain");
    #endif
      
    //  digitalWrite(CS_pin,LOW);
    delay(5);
    // SPI.transfer(0x61);//SYSGCAL
    //  digitalWrite(CS_pin,HIGH);
    //  delay(3500);

    //off-set
    #ifdef DEBUG
    Serial.println("setting off-set");
    #endif
    writeRegister(0x02,0b00101000);//ref 1
    //writeRegister(0x02,0b00110000);//internal ref
    digitalWrite(CS_pin,LOW);
    delay(5);
    SPI.transfer(0x62); // self-offset
    //SPI.transfer(0x60); // system-offset
    digitalWrite(CS_pin,HIGH);
    delay(3500);
}

void ADS1248::selfOffsetCal() {
    //off-set
    #ifdef DEBUG
    Serial.println("setting off-set");
    #endif
    writeRegister(0x02,0b00101000);//ref 1
    //writeRegister(0x02,0b00110000);//internal ref
    digitalWrite(CS_pin,LOW);
    delay(5);
    SPI.transfer(0x62); // self-offset
    //SPI.transfer(0x60); // system-offset
    digitalWrite(CS_pin,HIGH);
    delay(3500);
}


void ADS1248::readCalibrationRegisters() {
     
      Serial.println("ofc reg");
      Serial.println(readRegister(0x04));
      Serial.println(readRegister(0x05));
      Serial.println(readRegister(0x06));
      Serial.println("fsc reg");
      Serial.println(readRegister(0x07));
      Serial.println(readRegister(0x08));
      Serial.println(readRegister(0x09));
      
}


     
