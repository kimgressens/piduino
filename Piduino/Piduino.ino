/*
 * Piduino controller
 * Created by Beau Piccart & Kim Gressens
 * 8 May 2012
 */
#include "SPI.h"
#include "ADS1248.h"

#include <serialGLCD.h> // Include the library
#include <SoftwareSerial.h>
#include <TimerOne.h>
#include <PID_v1.h>
#include <Encoder.h>

SoftwareSerial LCDSerial(4,5);
serialGLCD lcd(&LCDSerial);

Encoder myEnc(3,4);
long oldPosition = -999;
long oldClickPosition = 0;

ADS1248 ads1248;

/*********** 
* GLOBAL VARS
************/
#define PIN_RELAY A0

#define SETPOINT_CURSOR 0
#define P_CURSOR 1
#define I_CURSOR 2
#define D_CURSOR 3
#define PR_CURSOR 4
#define CR_CURSOR 5
#define IT_CURSOR 6

#define NR_CURSOR_POSITIONS 7

double RTDSampleSum = 0;
int RTDSamples = 0;
double RTDTemp = -1;
double RTDR = -1;
double KTypeSampleSum = 0;
int KTypeSamples = 0;
int totalKSamples = 0;
double KTypeTemp = -1;

// Samples per second
int SPS_RTD = 640;
int SPS_K = 5;
// Gain
int Gain_RTD = 1;
int Gain_K = 64;

int RTD_Frequency = 30; // indicates how many samples of Thermocouple are taken before measuring the RTD

int sampleSource = 0; // 0 is no sample source, 1 is thermocouple, 2 is rtd

// PDM
float errorSum = 0;

/**********
* PID STUFF
***********/

//Define Variables we'll be connecting to
double Setpoint=40;
double Input, Output = 0;

//PID settings
double consKp=0.01, consKi=0, consKd=0;

// if true, discard the first thermocouple measurement
boolean discardFirstThermocoupleMeasurement = false;

//Specify the links and initial tuning parameters
PID myPID(&Input, &Output, &Setpoint, consKp, consKi, consKd, 1, DIRECT);

unsigned long lastPIDCompute;
int PIDComputeTime = 1000;

// OpenGL output serial
long lastSerialUpdate;
int SerialWindowSize=250;

// LCD update
long lastLCDUpdate;
int LCDUpdateWindow=1000;

// LCD cursor position
int cursorPosition = 3;

void setup(){
  Serial.begin(9600);
  LCDSerial.begin(115200);
  
  
  // Boot screen
  lcd.clearLCD();
  lcd.gotoLine(4);
  LCDSerial.print("    PIDuino v0.2");
  lcd.gotoLine(5);
  LCDSerial.print("   Calibrating ...");
  
  ads1248.init();
  attachInterrupt(0, readADS1248,FALLING);
  
  switchToRTD();
  
  // PID config
  myPID.SetPRange(10);
<<<<<<< HEAD
  myPID.SetControllableRegion(10);
=======
  myPID.SetControllableRegion(1);
>>>>>>> 17cbd08f1abfe8668d3552b73969d38acfb10133
  myPID.SetITime(3600.0);
  myPID.SetOutputLimits(0,1);
  //turn the PID on
  myPID.SetMode(AUTOMATIC);
  
  // PDM
  pinMode(PIN_RELAY, OUTPUT);    
  Timer1.initialize(40000);//25000); 
  Timer1.attachInterrupt(timerPDM); 
  
  lastPIDCompute = millis();
  lastSerialUpdate = millis();
  lastLCDUpdate = millis();
  setupLCD();
  //ads1248.selectAnalogInputPair(1);
  //ads1248.selfOffsetCal();
 // ads1248.setPGAGain(64);
}


void loop(){
   
  if (millis() - lastPIDCompute>PIDComputeTime) {
      myPID.Compute();
      lastPIDCompute = millis();
  }
  
   if(millis() - lastSerialUpdate > SerialWindowSize){
    lastSerialUpdate = millis();
    writeSerial();
 }
 
 if (millis() - lastLCDUpdate > LCDUpdateWindow) {
   lastLCDUpdate = millis();
   updateLCD();
 }
  
  long newPosition = myEnc.read();
  if (newPosition != oldPosition)  {
    if(newPosition%4==0 ){//possible new click state
       long newClickPosition = newPosition/4;
       // code to change cursor position
       /*if (newClickPosition>oldClickPosition) {
           cursorPosition++;
           cursorPosition = cursorPosition % NR_CURSOR_POSITIONS;
       } else if (newClickPosition<oldClickPosition) {
           cursorPosition--;
           cursorPosition = cursorPosition % NR_CURSOR_POSITIONS;
       }
       updateCursorOnLCD();*/
       
       // code to update setpoint
       
       if(newClickPosition>oldClickPosition){
          Setpoint = Setpoint-0.5*(newClickPosition-oldClickPosition);
       } else if(newClickPosition<oldClickPosition) {
          Setpoint = Setpoint-0.5*(newClickPosition-oldClickPosition);
       }
       updateSetpoint();
      
      oldClickPosition = newClickPosition;
      //updateEncoder();       

    }
   
    oldPosition = newPosition;
  }

}

void timerPDM()
{

    if (errorSum < 0 ){
       digitalWrite(PIN_RELAY,HIGH);
       errorSum+=1-Output;
    }else{
       digitalWrite(PIN_RELAY,LOW);
       errorSum-=Output;
    }

}

double convertPT100Temperature(double val) {
    //selectAnalogInputPair(0);

    //long val = read();


    // convert to R
    double max = 2147481344.0;
    double V = (val/max)*1.5;
   // return V;
    //double R = V/0.00015;

   // double R = (val / max)*1000.0; // (value / max ) * VBias / I
    double R = val; // *1000 is not done because we keep track of this in the ADCSamplesum
    
    // convert to T
    double R0 = 100;
    double A = 3.90830e-3;
    double B = -5.7750e-7;

    return (-A+sqrt(pow(A,2) - 4*B*(1-R/R0)))/(2*B);
}

double convertKTypeTemperature(double val) {
    double max = 2147481344.0*64;

    //val = (val - max/2);
    double toReturn = val / max * 1.5;

    return toReturn/0.000041; 
}

void switchToRTD() {
    sampleSource = 0;
    ads1248.setPGAGainAndSPS(Gain_RTD,SPS_RTD);
    ads1248.selectAnalogInputPair(0);
    discardFirstThermocoupleMeasurement = true;
    sampleSource = 2;
}

void switchToThermocouple() {
  sampleSource = 0;
  ads1248.selectAnalogInputPair(1);
  ads1248.setPGAGainAndSPS(Gain_K, SPS_K);
   sampleSource = 1;
}
void readADS1248()
{
  if (sampleSource == 1) {
    if (discardFirstThermocoupleMeasurement) {
        discardFirstThermocoupleMeasurement = false;
    } else {
        KTypeSampleSum += convertKTypeTemperature(ads1248.read());
    
      KTypeSamples++;
      
      if (KTypeSamples == SPS_K) {
         KTypeTemp = KTypeSampleSum/SPS_K;
         updateLCD(); 
         KTypeSampleSum = 0;
         KTypeSamples = 0;
      
         totalKSamples++;
         
         // set input for PID
         Input = RTDTemp + KTypeTemp;
      
         if (totalKSamples == RTD_Frequency) {
            totalKSamples = 0;
            switchToRTD(); 
         }
      }
    }
    
  } else if (sampleSource == 2) {
    double max = 2147481344.0;
   
    // keep val/(max*SPS) in sum
    RTDSampleSum+=ads1248.read()/(max*(SPS_RTD/1000.0)); // 2 instead of 2000 because we don't do *1000 in convertPT100Temperature
  
    //ADCsampleSum+=ads1248.readPT100Temperature();
    RTDSamples++;
    
      if(RTDSamples == SPS_RTD){
         RTDTemp = convertPT100Temperature(RTDSampleSum);
         RTDR = RTDSampleSum;
         updateLCD();
      
        RTDSampleSum = 0;
        RTDSamples = 0;
        //Serial.println(RTDTemp,5);
        switchToThermocouple();
        
      }
  }

}

void setupLCD() {
   lcd.clearLCD();
   lcd.gotoLine(1);
   LCDSerial.print("RTD      .....C");
   lcd.gotoLine(2);
   LCDSerial.print("Temp     ......C");
   lcd.gotoLine(3);
   LCDSerial.print("Output   .....%");
   lcd.gotoLine(4);
   LCDSerial.print("SetPoint .....C");
   lcd.gotoLine(6);
   LCDSerial.print("P=.... I=..... D=....");
   lcd.gotoLine(7);
   LCDSerial.print("PR=..C CR=.C It=....");   
   lcd.gotoLine(8);
   LCDSerial.print("Encoder ");
   LCDSerial.print(oldClickPosition); 
}

void updateCursorOnLCD() {
   updateSetpoint();
   updatePID();
   updatePRCRIt();
}

void setFontIfCursor(int currentPosition) {
   if (cursorPosition == currentPosition) {
       lcd.setFontMode(1);
   }
}

void unsetFontIfCursor(int currentPosition) {
  if (cursorPosition == currentPosition) {
      lcd.setFontMode(7);
  }
}

void eraseOrFillBasedOnCursor(int currentPosition,int x1, int y1, int x2, int y2) {
    if (cursorPosition == currentPosition) {
        lcd.drawFilledBox(x1,y1,x2,y2,0xFF);
    } else {
        lcd.eraseBlock(x1,y1,x2,y2);
    }
}

void updateRTD() {
   lcd.eraseBlock(getChar(9),getLine(0),getChar(14),getLine(1));
   lcd.gotoPosition(getChar(9),getLine(0));
   LCDSerial.print(checkLCDString(floatToString(RTDTemp,2),5));   
}



void updateTemp() {
   lcd.eraseBlock(getChar(9),getLine(1),getChar(15),getLine(2));
   lcd.gotoPosition(getChar(9),getLine(1));
   LCDSerial.print(checkLCDString(floatToString((RTDTemp+KTypeTemp),3),6));
}

void updateOutput() {
   lcd.eraseBlock(getChar(9),getLine(2),getChar(14),getLine(3));
   lcd.gotoPosition(getChar(9),getLine(2));
   LCDSerial.print(checkLCDString(floatToString((Output*100),0),5)); 
}

void updateSetpoint() {
   setFontIfCursor(SETPOINT_CURSOR);   
   eraseOrFillBasedOnCursor(SETPOINT_CURSOR,getChar(9),getLine(3),getChar(14),getLine(4));
   lcd.gotoPosition(getChar(9),getLine(3));
   LCDSerial.print(checkLCDString(floatToString(Setpoint,1),5));
   unsetFontIfCursor(SETPOINT_CURSOR);
}

void updatePID() {
  // P
  setFontIfCursor(P_CURSOR);
  eraseOrFillBasedOnCursor(P_CURSOR,getChar(2),getLine(5),getChar(6),getLine(6));
  lcd.gotoPosition(getChar(2),getLine(5));
  LCDSerial.print(myPID.GetKp());
  unsetFontIfCursor(P_CURSOR); 
  
  // I
  setFontIfCursor(I_CURSOR);
  eraseOrFillBasedOnCursor(I_CURSOR,getChar(9),getLine(5),getChar(14),getLine(6));
  lcd.gotoPosition(getChar(9),getLine(5));
  LCDSerial.print(myPID.GetKi(),3);
  unsetFontIfCursor(I_CURSOR);
  //D
  setFontIfCursor(D_CURSOR);
  eraseOrFillBasedOnCursor(D_CURSOR,getChar(17),getLine(5),getChar(21),getLine(6));
  lcd.gotoPosition(getChar(17),getLine(5));
  LCDSerial.print(myPID.GetKd());
  unsetFontIfCursor(D_CURSOR);
}

void updatePRCRIt() {
  // PR
  setFontIfCursor(PR_CURSOR);
  eraseOrFillBasedOnCursor(PR_CURSOR,getChar(3),getLine(6),getChar(7),getLine(7));
  lcd.gotoPosition(getChar(3),getLine(6));
  LCDSerial.print(myPID.GetPRange(),0);
  unsetFontIfCursor(PR_CURSOR);
  //CR
  setFontIfCursor(CR_CURSOR);
  eraseOrFillBasedOnCursor(CR_CURSOR,getChar(10),getLine(6),getChar(11),getLine(7));
  lcd.gotoPosition(getChar(10),getLine(6));
  LCDSerial.print(myPID.GetControllableRegion(),0);
  unsetFontIfCursor(CR_CURSOR);
  
  //It
  setFontIfCursor(IT_CURSOR);
  eraseOrFillBasedOnCursor(IT_CURSOR,getChar(16),getLine(6),getChar(20),getLine(7));
  lcd.gotoPosition(getChar(16),getLine(6));
  LCDSerial.print(myPID.GetITime()/60,1);
  unsetFontIfCursor(IT_CURSOR);
  LCDSerial.print("m");
}
void updateLCD() {
  updateRTD();
  updateTemp();
  updateOutput();
  updateSetpoint();
  updatePID();
  updatePRCRIt();
}

int getLine(int lineNr) {
  return lineNr*8;
}

int getChar(int charNr) {
  return charNr*6;
}

void updateEncoder() {
   lcd.drawFilledBox(getChar(8),getLine(7),getChar(11),getLine(8),0xFF);
   lcd.gotoPosition(getChar(8),getLine(7));
   lcd.setFontMode(1);
   //Serial.println(oldClickPosition);
   LCDSerial.print(oldClickPosition);
}

String checkLCDString(String temp, int length) {
  if (temp.length() > length) {
     temp = temp.substring(0,length);
   }
   
   return temp;
}

String floatToString(double number, uint8_t digits) 
{ 
  String resultString = "";
  // Handle negative numbers
  if (number < 0.0)
  {
     resultString += "-";
     number = -number;
  }

  // Round correctly so that print(1.999, 2) prints as "2.00"
  double rounding = 0.5;
  for (uint8_t i=0; i<digits; ++i)
    rounding /= 10.0;
  
  number += rounding;

  // Extract the integer part of the number and print it
  unsigned long int_part = (unsigned long)number;
  double remainder = number - (double)int_part;
  resultString += int_part;

  // Print the decimal point, but only if there are digits beyond
  if (digits > 0)
    resultString += "."; 

  // Extract digits from the remainder one at a time
  while (digits-- > 0)
  {
    remainder *= 10.0;
    int toPrint = int(remainder);
    resultString += toPrint;
    remainder -= toPrint; 
  } 
  return resultString;
}

void writeSerial(){
  Serial.print("T ");
  Serial.print(Input,10);
  Serial.println();
  
  Serial.print("O ");
  Serial.print(Output,8);
  Serial.println();
  
  Serial.print("PTerm ");
  Serial.print(myPID.GetPTerm(),6);
  Serial.println();
  
  Serial.print("ITerm ");
  Serial.print(myPID.GetITerm(),6);
  Serial.println();
  
  Serial.print("SP ");
  Serial.print(Setpoint,2);
  Serial.println();


  
}


